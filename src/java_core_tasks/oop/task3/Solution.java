package java_core_tasks.oop.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ok on 25.04.2017.
 */
public class Solution {
    static {
        //add your code here - добавьте код тут
        reset();
    }

    public static Flyable result;

    public static void reset() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line = br.readLine();
            if ("helicopter".equals(line)) result = new Helicopter();
            if ("plane".equals(line)){
                int num = Integer.parseInt(br.readLine());
                result = new Plane(num);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
