package java_core_tasks.oop.task1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by ok on 25.04.2017.
 */
public class Proger {
    public static void main(String[] args) throws Exception    {
        BufferedReader reader;
        Person person;
        String key;
        reader = new BufferedReader(new InputStreamReader(System.in));
        person = null;
        key = null;

        while(true) {
        key = reader.readLine();
            if("user".equals(key) || "looser".equals(key) || "coder".equals(key) || "proger".equals(key)){
                if (key.equals("user")){
                    person = new Person.User();
                }else if (key.equals("looser")){
                    person = new Person.Looser();
                }else if (key.equals("coder")){
                    person = new Person.Coder();
                }else if (key.equals("proger")){
                    person = new Person.Proger();
                }

                doWork(person);
            }
            else break;

        }
    }

    public static void doWork(Person person)    {
        // пункт 3
        if (person instanceof Person.User){
            ((Person.User) person).live();
        }else if (person instanceof Person.Looser){
            ((Person.Looser) person).doNothing();
        }else if (person instanceof Person.Coder){
            ((Person.Coder) person).coding();
        }else if (person instanceof Person.Proger){
            ((Person.Proger) person).enjoy();
        }
    }
}
