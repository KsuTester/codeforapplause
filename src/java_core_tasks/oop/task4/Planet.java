package java_core_tasks.oop.task4;

/**
 * Created by ok on 25.04.2017.
 */
public interface Planet {

    static String SUN = "sun";
    static String MOON = "moon";
    static String EARTH = "earth";
}
