package java_core_tasks.oop.task4;

/**
 * Created by ok on 25.04.2017.
 */
public class Sun implements Planet{

    private static Sun sunInstance;

    private Sun() {
    }

    public static Sun getInstance() {
        if (sunInstance == null){
            sunInstance = new Sun();
        }
        return sunInstance;
    }
}
