package java_core_tasks.oop.task4;

/**
 * Created by ok on 25.04.2017.
 */
public class Moon implements Planet{

    private static Moon moonInstance;

    private Moon(){
    }

    public static Moon getInstance(){
        if (moonInstance == null){
            moonInstance = new Moon();
        }
        return moonInstance;
    }
}
