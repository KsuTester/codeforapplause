package java_core_tasks.oop.task4;

/**
 * Created by ok on 25.04.2017.
 */
public class Earth implements Planet{

    private static Earth earthInstance;

    private Earth(){
    }

    public static Earth getInstance(){
        if (earthInstance == null){
            earthInstance = new Earth();
        }
        return earthInstance;
    }
}
