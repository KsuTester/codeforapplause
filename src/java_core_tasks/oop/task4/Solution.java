package java_core_tasks.oop.task4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ok on 25.04.2017.
 */
public class Solution {

    public static Planet thePlanet;

    static {
        readKeyFromConsoleAndInitPlanet();
    }

    public static void readKeyFromConsoleAndInitPlanet() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = null;
        try {
            line = br.readLine();
            if (line.equals(Planet.EARTH)){
                thePlanet = Earth.getInstance();
            } else if (line.equals(Planet.MOON)){
                thePlanet = Moon.getInstance();
            } else if (line.equals(Planet.SUN)){
                thePlanet = Sun.getInstance();
            } else thePlanet = null;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
