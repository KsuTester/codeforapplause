package java_core_tasks.oop.task02;

/**
 * Created by ok on 25.04.2017.
 */
public abstract class Money {

    private double amount;

    public Money(double amount)    {
        this.amount = amount;
    }

    public double getAmount(){
        return amount;
    }

    public abstract String getCurrencyName();
}
