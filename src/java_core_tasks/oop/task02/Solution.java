package java_core_tasks.oop.task02;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ok on 25.04.2017.
 */
public class Solution {
    public static void main(String[] args)    {
        Person ivan = new Person("Иван");
        for (Money money : ivan.getAllMoney())        {
            System.out.println(ivan.name + " имеет заначку в размере " + money.getAmount() + " " + money.getCurrencyName());
        }
    }

    static class Person    {
        public String name;
        private List<Money> allMoney;

        Person(String name)        {
            this.name = name;
            this.allMoney = new ArrayList<Money>();//!!
            allMoney.add(new Hrivna(50));
            allMoney.add(new Usd(200));
        }

        public List<Money> getAllMoney()        {//!!
            return allMoney;
        }
    }
}
