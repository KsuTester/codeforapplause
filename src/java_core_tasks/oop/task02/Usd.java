package java_core_tasks.oop.task02;

/**
 * Created by ok on 25.04.2017.
 */
public class Usd extends Money{

    public Usd(double amount) {
        super(amount);
    }

    @Override
    public String getCurrencyName() {
        return "USD";
    }
}
