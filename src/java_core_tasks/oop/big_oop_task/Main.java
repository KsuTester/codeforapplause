package java_core_tasks.oop.big_oop_task;

import java.util.GregorianCalendar;

/**
 * Created by ok on 25.04.2017.
 */
public class Main {

    public static void main(String[] args) {

        TrainDepot depot = new TrainDepot(10);

        depot.addTrain(new Train("Kharkiv", 127, new GregorianCalendar(2016, 05, 03, 22, 12), new Places(250, 100)));
        depot.addTrain(new Train("Nezhin", 12, new GregorianCalendar(2016, 05, 01, 20, 45), new Places(500)));
        depot.addTrain(new Train("Lviv", 45, new GregorianCalendar(2016, 04, 29, 02, 12), new Places(170, 80, 45)));
        depot.addTrain(new Train("Sumy", 91, new GregorianCalendar(2016, 05, 03, 15, 45), new Places(350, 170)));
        depot.addTrain(new Train("Poltava", 78, new GregorianCalendar(2016, 05, 23, 07, 40), new Places(400)));
        depot.addTrain(new Train("Odesa", 144, new GregorianCalendar(2016, 05, 10, 13, 50), new Places(300, 150, 60)));
        depot.addTrain(new Train("Odesa", 256, new GregorianCalendar(2016, 05, 11, 18, 20), new Places(270, 180, 30)));
        depot.addTrain(new Train("Dnepr", 23, new GregorianCalendar(2016, 05, 12, 23, 00), new Places(225, 130)));

        depot.showAllTrains(depot);
        depot.delTrainByNumber(78);
        depot.findByTrainNumber(127);
        depot.trainsByDestination("Odesa");
        depot.trainsByDestination("Rivne");
        depot.trainsByDestAndTime("Lviv", 2016, 04, 29, 12, 00);
        depot.trainsByDestAndCommonSeats("Nezhin", 10);

    }
}
