package java_core_tasks.oop.big_oop_task;

import java.util.GregorianCalendar;

/**
 * Created by ok on 25.04.2017.
 */
public class Train implements Comparable<Train>{

    private String destination;
    private int trainNumber;
    GregorianCalendar departure;

    public Train(String destination, int trainNumber, GregorianCalendar departure) {
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public GregorianCalendar getDeparture() {
        return departure;
    }

    @Override
    public String toString() {
        return "Train{" +
                "destination='" + destination + '\'' +
                ", trainNumber=" + trainNumber +
                ", departure='" + departure + '\'' +
                '}';
    }


    @Override
    public int compareTo(Train o) {
        if (this.getDestination().equals(o.getDestination())){
            return this.getDeparture().compareTo(o.getDeparture());
        }
        return this.getDeparture().compareTo(o.getDeparture());
    }
}
