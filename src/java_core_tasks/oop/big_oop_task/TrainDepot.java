package java_core_tasks.oop.big_oop_task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by ok on 25.04.2017.
 */
public class TrainDepot {

    private int trainDepotSize;
    private int actualSize;
    private Train[] trains;
    Calendar currentDate = new GregorianCalendar();

    public TrainDepot(int trainDepotSize) {
        this.trainDepotSize = trainDepotSize;
        this.actualSize = 0;
        this.trains = new Train[trainDepotSize];
    }

    public int getActualSize() {
        return actualSize;
    }

    @Override
    public String toString() {
        return "TrainDepot{" +
                "trains=" + "\n"+ Arrays.toString(trains) +
                '}';
    }

    public void addTrain(Train train){
        if (actualSize < trainDepotSize){
            trains[actualSize] = train;
            actualSize++;
        }
        else System.out.println("TrainDepot is full!!");
    }

    public void delTrainByNumber(int trainNumber){
        if (actualSize > 0){
            for (int i = 0; i < actualSize; i++) {
                if (trains[i].getTrainNumber() == trainNumber) {
                    actualSize--;
                    System.out.println("Train № " + trainNumber + " is deleted!");
                }
            }
        }
        else System.out.println("TrainDepot is empty!!");
    }


    public Train findByTrainNumber(int trainNumber) {
        for (int i = 0; i < actualSize; i++) {
            if (trains[i].getTrainNumber() == trainNumber){
                showTrain(trains[i]);
                return trains[i];
            }
        }
        System.out.println("There is no any train with " + trainNumber + " number!");
        return null;
    }

    public Train[] trainsByDestination(String destination){
        ArrayList<Train> res = new ArrayList<>();
        for (int i = 0; i < actualSize; i++) {
            if (trains[i].getDestination().equals(destination)){
                res.add(trains[i]);//
            }
        }
        Train[] res1 = new Train[res.size()];
        for (int j = 0; j < res.size(); j++) {
            res1[j] = res.get(j);
        }
        showTrain(res1);
        return res1;
    }

    public Train[] trainsByDestAndTime(String destination, int year, int month, int day, int hours, int minutes){
        ArrayList<Train> res2 = new ArrayList<>();
        GregorianCalendar requiredDate = new GregorianCalendar(year, month, day, hours, minutes);

        if (requiredDate.after(currentDate)){
            for (int i = 0; i < actualSize; i++) {

                if (trains[i].getDestination().equals(destination) && trains[i].getDeparture().equals(requiredDate)){
                    res2.add(trains[i]);
                }
            }
            Train[] res3 = new Train[res2.size()];
            for (int j = 0; j < res2.size(); j++) {
                res3[j] = res2.get(j);
            }
            showTrain(res3);
            return res3;
        }
        else {
            System.out.println("Wrong input! Enter valid date - that is after current date");
        }
        return null;
    }

    public Train[] trainsByDestAndCommonSeats(String destination, int commonSeats){
        ArrayList<Train> tr = new ArrayList<>();
        for (int i = 0; i < actualSize; i++) {
            if (trains[i].getDestination().equals(destination)){
                tr.add(trains[i]);
            }
        }
        Train[] trRes = new Train[tr.size()];
        for (int j = 0; j < tr.size(); j++) {
            trRes[j] = tr.get(j);
        }
        showTrain(trRes);
        return trRes;
    }

    public void showAllTrains(TrainDepot depot){
        System.out.println(depot + "\n");
    }

    public void showTrain(Train[] train){
        for (int i = 0; i < train.length; i++) {
            System.out.println(train[i] );
        }
        System.out.println();
    }

    public static void showTrain(Train tr) {
        System.out.println(tr);
        System.out.println();
    }

}
