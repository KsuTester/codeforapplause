package java_core_tasks.oop.big_oop_task;

/**
 * Created by ok on 25.04.2017.
 */
public class Places {

    int commonSeats;
    int reservedSeats;
    int compartment;
    int lux;

    public Places(int commonSeats) {
        this.commonSeats = commonSeats;
    }

    public Places(int reservedSeats, int compartment) {
        this.reservedSeats = reservedSeats;
        this.compartment = compartment;
    }

    public Places(int reservedSeats, int compartment, int lux) {
        this.reservedSeats = reservedSeats;
        this.compartment = compartment;
        this.lux = lux;
    }

    public int getCommonSeats() {
        return commonSeats;
    }

    public int getReservedSeats() {
        return reservedSeats;
    }

    public int getCompartment() {
        return compartment;
    }

    public int getLux() {
        return lux;
    }

    @Override
    public String toString() {
        return "Places{" +
                "commonSeats=" + commonSeats +
                ", reservedSeats=" + reservedSeats +
                ", compartment=" + compartment +
                ", lux=" + lux +
                '}';
    }
}
