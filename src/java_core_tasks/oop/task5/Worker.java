package java_core_tasks.oop.task5;

import java.util.GregorianCalendar;

/**
 * Created by ok on 25.04.2017.
 */
public class Worker {

    private String name;
    private String surname;
    private GregorianCalendar hireDate;
    private double salary;

    public Worker(String name, String surname, int hDay, int hMonth, int hYear, double salary) {
        this.name = name;
        this.surname = surname;
        this.hireDate = new GregorianCalendar(hYear, hMonth, hDay);
        this.salary = salary;
    }

    public String getFullName() {
        return surname + " " + name;
    }

    public double getSalary() {
        return salary;
    }
}
