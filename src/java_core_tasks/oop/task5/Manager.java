package java_core_tasks.oop.task5;

/**
 * Created by ok on 25.04.2017.
 */
public class Manager extends Worker{

    private double bonus;

    public Manager(String name, String surname, int hDay, int hMonth, int hYear, double salary, double bonus) {
        super(name, surname, hDay, hMonth, hYear, salary);
        this.bonus = bonus;
    }

    @Override
    public double getSalary() {
        return super.getSalary() + this.bonus;
    }

    public double getBonus() {
        return bonus;
    }

}
