package java_core_tasks.oop.task5;

/**
 * Created by ok on 25.04.2017.
 */
public class Main {

    public static void main(String[] args) {

        Worker[] workers = new Worker[5];
        workers[0] = new Worker("Mike", "Simpson", 10, 10, 2010, 3000.0);
        workers[1] = new Worker("Bill", "Smith", 25, 12, 2005, 4000.0);
        workers[2] = new Manager("Rob","Thompson", 30, 03, 2008, 5000.0, 2000.0);
        workers[3] = new Worker("Fill", "Brown", 10, 10, 2010, 3500.0);
        workers[4] = new Manager("John", "Black", 25, 12, 2005, 6000.0, 1500.0);

        for (Worker w : workers) {
            System.out.println(w.getFullName() + " - " + w.getSalary());
        }
        System.out.println();

        for (Worker w : workers) {
            if (w instanceof Manager) {
                double bon = ((Manager) w).getBonus();
                System.out.println(w.getFullName() + " - " + bon);
            }
        }
        System.out.println();

        System.out.println("Average bonus: " + getAverageBonus(workers));
        System.out.println("Average workers salary: " + getAverageWorkersSalary(workers));
        System.out.println("Average managers salary: " + getAverageManagersSalary(workers));
    }

    public static double getAverageBonus(Worker[] workers) {

        double sum = 0;
        int countManagers = 0;
        for (Worker worker : workers) {
            if (worker instanceof Manager) {
                sum += ((Manager) worker).getBonus();
                countManagers++;
            }
        }
        return sum/countManagers;
    }

    public static double getAverageWorkersSalary(Worker[] workers) {

        double sum = 0;
        int countWorkers = 0;
        for (Worker worker : workers) {
            if (!(worker instanceof Manager)) {
                sum += worker.getSalary();
                countWorkers++;
            }
        }
        return sum / countWorkers;
    }

    public static double getAverageManagersSalary(Worker[] workers) {

        double sum = 0;
        int countManagers = 0;
        for (Worker worker : workers) {
            if (worker instanceof Manager) {
                sum += ((Manager) worker).getSalary();
                countManagers++;
            }
        }
        return sum/countManagers;
    }
}
