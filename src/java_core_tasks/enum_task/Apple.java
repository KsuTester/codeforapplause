package java_core_tasks.enum_task;

/**
 * Created by ok on 25.04.2017.
 */
public enum Apple {

    Jonathan, Golden, RedDel, Winesap, Cortland
}
