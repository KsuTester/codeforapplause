package java_core_tasks.enum_task;

/**
 * Created by ok on 25.04.2017.
 */
public class Main {

    public static void main(String[] args) {
        Apple apple1, apple2, apple3;
        System.out.println();

        for (Apple ap : Apple.values()) {
            System.out.println(ap + " " + ap.ordinal());
        }
        apple1 = Apple.RedDel;
        apple2 = Apple.Golden;
        apple3 = Apple.Cortland;
        System.out.println();


        if (apple1.compareTo(apple2) < 0){
            System.out.println(apple1 + " comes before " + apple2);
        }
        if (apple1.compareTo(apple2) > 0){
            System.out.println(apple2 + " comes before " + apple1);
        }
        if (apple1.compareTo(apple3) == 0){
            System.out.println(apple1 + " equals " + apple3);
        }
        System.out.println();


        if (apple1.equals(apple2)){
            System.out.println("Error!");
        }
        if (apple1.equals(apple3)){
            System.out.println(apple1 + " equals " + apple3);
        }
        if (apple1 == apple2){
            System.out.println(apple1 + " == " + apple2);
        }
    }
}
