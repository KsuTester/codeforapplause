package java_core_tasks.streams_task;

import java.io.Serializable;

/**
 * Created by ok on 25.04.2017.
 */
public class Goods implements Serializable{

    private String name;
    private int price;

    public Goods(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
