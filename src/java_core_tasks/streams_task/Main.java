package java_core_tasks.streams_task;

import java.io.*;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ok on 25.04.2017.
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        int a = 1;
        int b = 1;
        List<Goods> list;
        list = loadList();

        for (Goods goods : list) {
            System.out.println(goods);
        }

        System.out.println("Input name and price");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String[] gd = s.split(" ");

        if (gd.length==2){
            list.add(new Goods(gd[0], Integer.parseInt(gd[1])));
            System.out.println("update list");
            for (Goods item : list) {
                System.out.println(item);
            }
            saveList(list);
        }
    }

    static void saveList(List<Goods> list) throws IOException {
        FileOutputStream foStream = new FileOutputStream("goods.dat");
        ObjectOutputStream ooStream = new ObjectOutputStream(foStream);
        ooStream.writeObject(list);
        ooStream.flush();
        foStream.flush();
        ooStream.close();
    }

    public static List loadList() throws IOException, ClassNotFoundException {
        FileInputStream fStream = new FileInputStream("goods.dat");
        ObjectInputStream oStream = new ObjectInputStream(fStream);

        List<Goods> list = (List<Goods>) oStream.readObject();

        fStream.close();
        oStream.close();
        return list;

    }
}
