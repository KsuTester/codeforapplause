package java_core_tasks.threads.task1;

/**
 * Created by ok on 25.04.2017.
 */
public class ThreadTwo implements Runnable {

    private int count;
    public ThreadTwo(int n) {
        count = n;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+ " " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
