package java_core_tasks.threads.task1;

/**
 * Created by ok on 25.04.2017.
 */
public class Main {

    public static void main(String[] args) {

        ThreadOne t1 = new ThreadOne(7);
        t1.start();

        ThreadTwo t2 = new ThreadTwo(9);
        Thread t = new Thread(t2);
        t.start();
    }
}
