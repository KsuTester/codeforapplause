package java_core_tasks.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by ok on 25.04.2017.
 */
public class TestHashMap {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("january" ,31);
        map.put("february" ,29);
        map.put("march",31);
        map.put("april", 30);
        map.put("april", 60);
        System.out.println(map);

        System.out.println(map.get("march"));
        map.remove("february");
        System.out.println(map);

        Set<String> setOfKeys = map.keySet();
        System.out.println(setOfKeys);

        for (String s : setOfKeys) {
            System.out.println(s);
        }
        System.out.println("------------------");
        Collection<Integer> setOfValues = map.values();
        System.out.println(setOfValues);
        for (Integer value : setOfValues) {
            System.out.println(value);
        }
        System.out.println("------------");
        Set<Map.Entry<String, Integer>> setOfKV = map.entrySet();

        for (Map.Entry<String, Integer> strInt : setOfKV) {
            System.out.println(strInt.getKey() + "=" + strInt.getValue());
        }
        System.out.println("-------------");

        System.out.println(map.containsKey("january"));
        System.out.println(map.containsValue(33));

    }
}
