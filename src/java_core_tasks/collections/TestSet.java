package java_core_tasks.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by ok on 25.04.2017.
 */
public class TestSet {

    public static void main(String[] args) {
        Set<Integer> set = new HashSet<Integer>();
        set.add(111);
        set.add(111);
        set.add(333);

        Iterator<Integer> iter = set.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
        System.out.println("-------------");
        for (Integer i : set) {
            System.out.println("Output for: " + i);
        }
        System.out.println("-------------");
        System.out.println(set);

        set.remove(111);
        set.clear();

        set.add(123);
        set.add(333);
        System.out.println(set.contains(123));
        System.out.println("set " + set);

        Set<Integer> set2 = new HashSet<Integer>();
        set2.add(123);
        System.out.println("set2 " + set2);

        set.removeAll(set2);
        for (Integer i : set) {
            System.out.println("Output for: " + i);
        }
    }
}
